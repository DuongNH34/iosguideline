# Swift Development Guide

## Coding Style

Basically, we can follow 

* [GitHub's Swift Coding Style Guild](https://github.com/github/swift-style-guide)
* [Raywenderlich 's Swift Coding Style Guild](https://github.com/raywenderlich/swift-style-guide)

## Tool You need to install

* [SwiftLint](https://github.com/realm/SwiftLint)
* [synx](https://github.com/venmo/synx)
* [Tayler](https://tailor.sh/)
* [Cocoalumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack)
* [Reveal](http://revealapp.com/)
 
## Things you should do before pull request

* Use [SwiftLint](https://github.com/realm/SwiftLint) to check if it follow the coding style
* Sync Xcode project physical directory structure with Xcode group
  * You can use [synx](https://github.com/venmo/synx)
* Use [Tayler](https://tailor.sh/) to ensure code clean


## Utilities(App)
* Use [MarkDown](http://macdown.uranusjr.com/) to create/read .md file
* [PopClip](https://pilotmoon.com/popclip/) : text edit popup
* [Copy&Paste](): Store all copied and paste clipboard
* [iTools](): Read information ios device
* [Link Folder](https://drive.google.com/open?id=0BxNQMghEViH_TExCdlRjLUx5N2M)

## Xcode plugins

* Plugin thay đổi kích cỡ font size : <br>
<https://github.com/zats/AdjustFontSize-Xcode-Plugin>
* Plugin hiển thị màu sắc uicolor :<br>
<https://github.com/NorthernRealities/ColorSenseRainbow>
* Plugin refactor name của swift :<br>
<https://github.com/johnno1962/Refactorator>
* Plugin auto hightlight symbol :<br>
<https://github.com/chiahsien/AutoHighlightSymbol>
* Plugin edit podfile in Xcode <br>
<https://github.com/kattrali/cocoapods-xcode-plugin>
* Plugin smart panel<br>
<https://github.com/chaingarden/DBSmartPanels/>
* Plugin remove default margin<br>
<https://github.com/mshibanami/DefaultMarginDisabler>
* Plugin load image name<br>
<https://github.com/ksuther/KSImageNamed-Xcode/>

* TẤT CẢ ĐỀU CÓ THỂ CÀI ĐẶT QUA ALCATRAZ :
<https://github.com/alcatraz/Alcatraz>


#Ebook

*Một số sách ebook tổng hợp của group ios developers vn (có swift apprentice , ios 9 , ios animation của ray , appcoda…… )

<https://docs.google.com/spreadsheets/d/1u5--q0p9wFaSZ3r_a5sipCOYAmIOODXwlsS4b4W4Uvs/edit?usp=sharing>