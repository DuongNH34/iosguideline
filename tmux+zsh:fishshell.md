# Welcome to ZSH/Fish + tmux
Yeah tự nhiên buổi trưa uống nhiều cafe nên giờ hơi get high quá mức nên rảnh rỗi viết cái post chia sẻ một số plugin hay về **`zsh/shell`** và **`tmux`** (ví dụ như suggest các câu lệnh đã có để không phải gõ lại,hightlight câu lệnh ,hiển thị đồng hồ trong terminal ,show bài nhạc đang chạy của itunes .......) fishshell thì có sẵn auto-suggestion và auto-hightlight còn zsh thì phải cài thêm 2 plugin này

----------
[TOC]
## Cài đặt
1. Đầu tiên ta phải cài `zsh/fishshell` đã  
  * [Hướng dẫn cài ZSH](https://gist.github.com/derhuerst/12a1558a4b408b3b2b6e)
  * [Hướng dẫn cài fishshell](https://gist.github.com/derhuerst/12a1558a4b408b3b2b6e)
2. Sau đấy là cài đặt `oh-my-zsh` với `zsh` và `oh-my-fish(omf)` với `fishshell` (đại loại kiểu pakage manager / extension manager của 2 shell)
  * [Hướng dẫn cài oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
  * [Hướng dẫn cài omf](https://github.com/oh-my-fish/oh-my-fish)
3. À quên còn phải cài đặt tmux nữa :D (Tmux là một Terminal Multiplexer - "bộ ghép kênh". Nó cho phép bạn chuyển qua lại giữa các chương trình độc lập ngay trên một terminal, tách các chương trình ra một terminal riêng mà vẫn giữ được trạng thái hoạt động của chúng blah blah)
  * MacOS : brew install tmux
  * Ubuntu : update sau ......
  * Window : update sau .......

## Setting ZSH/fishshell

- zsh để config trong ~/.zshrc đây là config của mình :

``` ruby
# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/phonex/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git tmux sublime brew docker osx zsh-autosuggestions zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

```

Đại loại là mình dùng `theme agnoster` và dùng các plugin `git tmux sublime brew docker osx zsh-autosuggestions zsh-syntax-highlighting`
  - cái agnoster dùng power line font nên các bạn phải cài ở đây nhé [Powerline font](https://github.com/powerline/fonts)
  - cái `auto-suggestion` không có sẵn trong oh-my-zsh nên phải cài ngoài ở đây [auto-suggestion](https://github.com/zsh-users/zsh-autosuggestions)
  - tương tự với `auto-hightlight` [auto hightlight](https://github.com/zsh-users/zsh-syntax-highlighting)

- fishshell thì mình cài thêm cái sublime,weather:
  * omf install sublime
  * omf install weather

## Setting tmux

``` ruby
# 1. Install Homebrew (https://github.com/mxcl/homebrew)
# 2. brew install zsh
# 3. Install OhMyZsh (https://github.com/robbyrussell/oh-my-zsh)
# 4. brew install reattach-to-user-namespace --wrap-pbcopy-pbpaste && brew link reattach-to-user-namespace
# 5. Install iTerm2
# 6. In iTerm2 preferences for your profile set:
#      Character Encoding: Unicode (UTF-8)
#      Report Terminal Type: xterm-256color
# 7. Put itunesartist and itunestrack into PATH
#
#
# Usage:
# - Prefix is set to Ctrl-a (make sure you remapped Caps Lock to Ctrl)
# - All prefixed with Ctrl-a
#   - Last used window: /
#   - Last used pane:   ;
#   - Vertical split:   v
#   - Horizontal split: s
#   - Previous window:  [
#   - Next window:      ]
#   - Choose session:   Ctrl-s
#   - Quick window:     Ctrl-q

#set-option -g default-command "reattach-to-user-namespace -l zsh"

### LOOK & FEEL ###
set -g default-terminal "xterm-256color"

# default statusbar colors
set-option -g status-bg colour235
set-option -g status-fg colour179
set-option -g status-attr default

# default window title colors
set-window-option -g window-status-fg colour244
set-window-option -g window-status-bg default

# active window title colors
set-window-option -g window-status-current-fg colour166
set-window-option -g window-status-current-bg default
set-window-option -g window-status-current-attr bright

# pane border
set-option -g pane-border-fg colour235
set-option -g pane-active-border-fg colour240

# pane number display
set-option -g display-panes-active-colour colour33
set-option -g display-panes-colour colour166

# clock
set-window-option -g clock-mode-colour colour64

# status bar right contents
set -g status-right-length 65
set -g status-right "#[fg=colour187][#(~/Documents/tmux_config/itunesartist) - #(~/Documents/tmux_config/itunestrack)] #[fg=default][%H:%M %e-%b-%Y]"
set -g status-interval 5

#set-option -g mouse-select-pane on
#set-option -g mouse-select-window on
#set-option -g mode-mouse on

#set-window-option -g utf8 on

set-option -g status-keys vi
set-option -g mode-keys vi

#no command delay
set -sg escape-time 0

#count windows and panes from 1
set -g base-index 1
setw -g pane-base-index 1


#using C-a as prefix
unbind C-b
set-option -g prefix C-a
bind C-a send-prefix

unbind /
bind / last-window

unbind %
bind - split-window -v
unbind '"'
bind \ split-window -h

bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

```

1. Để hiểu rõ hơn về các lệnh tmux các bạn đọc qua bài này nhé [Nghịch tmux](http://kipalog.com/posts/Nghich-tmux)
  * ở đây mình dùng Ctrl-a thay cho phím Ctrl-b mặc định tmux
  * dùng Ctrl-a-\ thay " để split Vertical
  * dùng Ctrl-a-\ thay " để split Horizontal
  * dùng Ctrl-a-h j k l để chuyển pane

2. itnuesartist code show tên tác giả bài nhạc đang nghe

``` ruby
#!/usr/bin/osascript
on run
  set info to ""
  tell application "System Events"
    set num to count (every process whose name is "iTunes")
  end tell
  if num > 0 then
    tell application "iTunes"
      if player state is playing then
        set info to artist of current track
      end if
    end tell
  end if
  return info
end run
```
3. itunestrack code show tên bài hát đang nghe

``` ruby
on run
  set info to ""
  tell application "System Events"
    set num to count (every process whose name is "iTunes")
  end tell
  if num > 0 then
    tell application "iTunes"
      if player state is playing then
        set info to name of current track
      else
        set info to "Not Playing."
      end if
    end tell
  end if
  return info
end run
```

4. mình để 2 file trên trong ``"~/Documents/tmux_config/"``

ok đến phần demo rồi keke :D
![picture]( https://bitbucket.org/DuongNH34/iosguideline/raw/master/terminal.png)
